var gulp = require('gulp');
var less = require('gulp-less');
var path = require('path');

var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var minify=require('gulp-minify');
var uglify = require('gulp-uglify');
gulp.task('gridless', function () {
  return gulp.src('./src/content/grid/_grid.less')
    .pipe(less({
      paths: [ path.join(__dirname, 'less', 'includes') ]
    }))
    .pipe(gulp.dest('./dist'));
});

gulp.task('albumcss', function () {
  return gulp.src('./src/content/pages/album.less')
    .pipe(less({
      paths: [ path.join(__dirname, 'less', 'includes') ]
    }))
    .pipe(gulp.dest('./dist'));
});

gulp.task('logincss', function () {
  return gulp.src('./src/content/pages/login.less')
    .pipe(less({
      paths: [ path.join(__dirname, 'less', 'includes') ]
    }))
    .pipe(gulp.dest('./dist'));
});



gulp.task('corejs', function () {
  return gulp.src([
      
      'src/js/core/domhelper.js',
      'src/js/core/ajax.js',
      'src/js/core/urlhelper.js'
  ])
      .pipe(concat('core.js'))
      .pipe(minify())
      .pipe(gulp.dest('dist'))
});

gulp.task('albumjs', function () {
  return gulp.src([
      'src/js/pages/album.js'
  ])
      .pipe(minify())
      .pipe(gulp.dest('dist'))
});

