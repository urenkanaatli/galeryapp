window.onload = function () {
    var urlParams = new URLSearchParams(window.location.search);
    var albumid = urlParams.get('albumid');
    if (albumid > 0) {
        getImages(albumid);
    }
};

window.onpopstate = function (e) {
    if (e.state) {
        document.getElementById("photoarea").innerHTML = e.state.html;
        document.title = e.state.pageTitle;
        document.getElementsByClassName('spinner-border').remove()
    } else {
        document.getElementById("photoarea").innerHTML = "";
    }
};


function getImages(id, owner) {

    [].forEach.call(document.getElementsByClassName("galerya"), function (el) {
        el.classList.remove("is-active");
    });

    owner.classList.add("is-active");
    document.getElementsByClassName('bigimagecontainer')[0].innerHTML = "";
    var loader = '<div class="loaderarea"><div class="spinner-border text-primary" role="status"></div></div>';
    document.getElementById("photoarea").innerHTML = loader;
    ajax("GET", `/photos?albumid=${id}`).then(function (result) {
        document.getElementById("photoarea").innerHTML = result;

        var allImages = document.getElementById('photoarea').getElementsByTagName("img");

        for (var i = 0; i < allImages.length; i++) {
            allImages[i].onload = function () {

                this.parentNode.getElementsByClassName('spinner-border').remove();
                this.parentNode.classList.remove('inloading');


            };
        }
        window.history.pushState({ "page": "photo", "html": result, "pageTitle": `Galeri-${id}` }, "", `?albumid=${id}`);

    }).catch(function (hata) {

    })
}


function getBigImage(owner) {
    console.log("bubura");
    var activeImage = document.querySelector('.card.is-active');
    if (activeImage) 
    {
        activeImage.classList.remove("is-active");
        activeImage.getElementsByClassName('checkmark').remove();
    }
    owner.classList.add("is-active");
    owner.getElementsByClassName('imagewaitloader')[0].innerHTML='<span class="checkmark"></span>';

    var bigImage = owner.getAttribute("data-url");
    document.getElementsByClassName('bigimagecontainer')[0].innerHTML = `<img src='${bigImage}'></img>`;

}