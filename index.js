var express=require('express');
var wiki = require('./Routing.js');
var bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const session = require('express-session');
var app=express();
app.use(cookieParser());
app.use(session({
    secret: "app_01020304",
    resave: false,
    saveUninitialized: false
}));
app.use(express.static(__dirname + '/dist'));
app.use('/static', express.static(__dirname + '/dist'));
app.set("view engine", "pug");
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use('/', wiki);





var server=app.listen(3000,function() {});