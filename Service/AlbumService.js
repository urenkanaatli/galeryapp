require('es6-promise').polyfill();
require('isomorphic-fetch');

const SERVICEURL_ALBUM_URL = "https://jsonplaceholder.typicode.com/albums";
const SERVICEURL_IMAGE_URL = `https://jsonplaceholder.typicode.com/photos`;


exports.getAlbums = async function () {
    let response = await fetch(SERVICEURL_ALBUM_URL);
    let data = await response.json()
    return data;
};

exports.getImages = async function (albumid) {
    let url = SERVICEURL_IMAGE_URL + `?albumId=${albumid}`;
    let response = await fetch(url);
    let data = await response.json();

    return data;


};