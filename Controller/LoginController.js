const pug = require('pug');
const ERRORS=require('../Responses/Errors');

exports.isAuthenticated=function(req, res, next) {	
	if (req.session &&  req.session.user)
	return next();

	res.redirect('/Login');
  }

exports.getlogin=function(request,response)
{
    response.render("Login");
}
exports.logout=function(request,response)
{
    request.session.destroy(function(){
         response.redirect('/Login');     
     });
     
        
}

exports.login=function (request, response) {
    var username = request.body.username;
    var password = request.body.password;

    if(username=="a@a.com" &&password=="123123")
    {
        var newUser = {username: username, password: password};
        request.session.user = newUser;
        
        response.redirect('/');
    }else
    {
        response.render("Login", { error: ERRORS.INVALID_LOGIN.MESSAGE, username: username });
      
    }
}