const pug = require('pug');
var albumserive = require('../Service/AlbumService');
var ERRORS = require('../Responses/Errors');
const querystring = require('querystring');

exports.getAlbums = (request, response) => {
    albumserive.getAlbums().then(result => {
        if (result && result.length > 10)
        result = result.slice(0, 10);
        response.render("Galery", { albums: result });
    }).catch(t => {
         response.statusCode =500;
        response.send({result:ERRORS.SISTEM_ERROR});
    })

};
exports.photos = (request, response) => {
    let albumid = request.query.albumid;
    albumserive.getImages(albumid).then(result => {      
        if (result && result.length > 10)
            result = result.slice(0, 10);

            response.render("GaleryItem", {result:ERRORS.SUCCESS ,photos: result });
    }).catch(t => {
        //hata loglanmalı
        response.statusCode =500;
        response.send({result:ERRORS.SISTEM_ERROR});
    })

};