var express = require('express'); 
var router = express.Router();
var path = require('path');

var albumcontroller = require('./Controller/MyAlbumsConttroller');
var logincontroller = require('./Controller/LoginController');


router.get('/myalbums',logincontroller.isAuthenticated, albumcontroller.getAlbums);
router.get('/photos',logincontroller.isAuthenticated,albumcontroller.photos);
router.get('/',logincontroller.isAuthenticated,(req,res)=>{res.redirect('/Galery');})
router.get('/Galery',logincontroller.isAuthenticated,albumcontroller.getAlbums)
router.post('/Login',logincontroller.login);
router.get('/Login',logincontroller.getlogin);
router.get('/Logout',logincontroller.logout);




module.exports = router;